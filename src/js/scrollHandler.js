// Gestion du comportement au scroll de la navbar

(function() {
  'use strict';

  const nav = document.getElementById('test');

  console.log(nav);

  console.log(nav.classList.contains('navbar--fixed-top'));

  //Tableau de constantes de classes CSS pour cibler le type de comportement de la navbar
  const classesCss = {
    FIXED_NAV : 'navbar--fixed-top'
  }

  function getNavbarType(){

    if(nav.classList.contains(classesCss.FIXED_NAV)){

      console.log('Nav fixée');

    }else{

      console.log('nav pas fixée');

    }

  }

  getNavbarType();

  })();
