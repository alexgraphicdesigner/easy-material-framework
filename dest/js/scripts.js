"use strict";

// Responsive Nav
(function () {
  'use strict';
})();
"use strict";

// Effet d'ondulation
(function () {
  'use strict'; //On récupère tous les élements ayant la classe ".btn" et on les stocke dans la variable, on initialise rippleContainer et button.

  var buttons = document.querySelectorAll('.ripple-enabled'),
      button,
      rippleContainer; //Pour chaque élément du tableau de constantes "buttons" retourné, on crée une 'div' avec la classe '.ripple__container'.

  for (var i = 0; i < buttons.length; i++) {
    button = buttons[i];
    rippleContainer = document.createElement('div');
    rippleContainer.className = 'ripple__container';
    button.rippleContainer = rippleContainer;
    button.appendChild(rippleContainer); //Evènement de déclenchement de la fonction RippleEffect

    button.addEventListener('mousedown', rippleEffect);
    button.addEventListener('mouseup', debounce(cleanRipple, 2000));
  }

  ; //Fonction RippleEffect qui va "créer" l'ondulation.

  function rippleEffect(e) {
    // On crée un span au clic, on récupère la position en hauteur et largeur du clic
    var ripple = document.createElement('span'),
        size = this.offsetWidth,
        pos = this.getBoundingClientRect(),
        posX = e.pageX - pos.left - size / 2,
        posY = e.pageY - pos.top - size / 2; //On ajoute une classe "ripple__effect" au clic, et on rattache ce span en tant qu'élément enfant de l'élement sur lequel on vient de cliquer.

    ripple.classList.add('ripple__effect');
    this.rippleContainer.appendChild(ripple); //On ajoute le style au span ainsi créé

    ripple.style.top = posY + 'px';
    ripple.style.left = posX + 'px';
    ripple.style.height = size + 'px';
    ripple.style.width = size + 'px';
  }

  ; //Fonction qui supprime le span ".ripple__effect"

  function cleanRipple() {
    while (this.rippleContainer.firstChild) {
      this.rippleContainer.removeChild(this.rippleContainer.firstChild);
    }
  }

  ; //Déclenche la fonction "cleanRipple" après un certain délai, mais se réinitialise si on demande une nouvelle execution avant la fin du délai défini

  function debounce(callback, delay) {
    var timer;
    return function () {
      var args = arguments,
          context = this;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, delay);
    };
  }

  ;
})();
"use strict";

// Gestion du comportement au scroll de la navbar
(function () {
  'use strict';

  var nav = document.getElementById('test');
  console.log(nav);
  console.log(nav.classList.contains('navbar--fixed-top')); //Tableau de constantes de classes CSS pour cibler le type de comportement de la navbar

  var classesCss = {
    FIXED_NAV: 'navbar--fixed-top'
  };

  function getNavbarType() {
    if (nav.classList.contains(classesCss.FIXED_NAV)) {
      console.log('Nav fixée');
    } else {
      console.log('nav pas fixée');
    }
  }

  getNavbarType();
})();
"use strict";

/* Tableau Responsive */
(function () {
  'use strict';

  document.querySelectorAll('table').forEach(function (table) {
    var labels = [];
    table.querySelectorAll('th').forEach(function (th) {
      labels.push(th.innerText);
    });
    table.querySelectorAll('td').forEach(function (td, i) {
      td.setAttribute('data-label', labels[i % labels.length]);
    });
  });
})();