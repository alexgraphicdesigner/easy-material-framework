//Gulpfile.js d'Easyframework

//On appelle Gulp
const {src, dest, task, series, parallel, watch} = require("gulp");

//Plugin pour la compilation du SASS
const sass = require("gulp-sass"),
      del = require("del"),
      plumber = require("gulp-plumber"),
      autoprefixer = require("autoprefixer"),
      postcss = require('gulp-postcss'),
      cssbeautify = require("gulp-cssbeautify"),
      uglify = require ("gulp-uglify"),
      cleanCSS = require ("gulp-clean-css"),
      concat = require("gulp-concat"),
      babel = require("gulp-babel"),
      browserSync = require("browser-sync").create(),
      browserify = require("browserify");



//Chemins fichiers CSS et JS
const paths = {

    srcBase : 'src/scss/*.scss',
    srcComposants : 'src/scss/**/*.scss',

    srcJS : 'src/js/*.js',
    srcJSComposants : 'src/js/**/*.js',

    srcHTML: '*.html',

    distCss : 'dest/css',
    distJS : 'dest/js'
}

//----------------------------------------------------------------
// TACHES DE DEV
//----------------------------------------------------------------


//On nettoie les repertoires de destination
function clean(){
  return del([paths.distCss, paths.distJS])
}

//BrowserSync, création d'un serveur local
function gulpBrowserSync(){
     browserSync.init({
         server: {
            baseDir: "./"
         }
     });
}

//Refresh automatique à chaque enregistrement
function gulpReload(done){
  browserSync.reload();
	done();
}

//Compilation du SASS de developpement
function gulpSass(done){
  return src(paths.srcBase, {sourcemaps: true})
  .pipe(plumber())
  .pipe(sass({
    errLogToConsole: true
  }))
  .on( 'error', console.error.bind( console ) )
  .pipe(postcss([ autoprefixer() ]))
  .pipe(cssbeautify({indent: '  '})) //indentation de deux espaces
  .pipe(dest(paths.distCss, {sourcemaps: '.'}))
  .pipe(browserSync.stream());
  done();
}

//Compilation du Javascript de developpement
function gulpJS(done){
  return src(paths.srcJSComposants)
  .pipe(plumber())
  .pipe(babel())
  .pipe(concat('scripts.js'))
  .pipe(dest(paths.distJS))
  .pipe(browserSync.stream() );
  done();
}

//On écoute les modifications de fichiers pour rechargement de la page automatique
function gulpWatch(){
  watch([paths.srcBase, paths.srcComposants], series(gulpSass, gulpReload));
  watch(paths.srcJSComposants, series(gulpJS, gulpReload));
  watch(paths.srcHTML, series(gulpReload));
}

//On liste les tâches
task("gulpSass", gulpSass);
task("gulpJS", gulpJS);
task("default", series(clean, parallel(gulpSass, gulpJS)));
task("build-dev", parallel(gulpBrowserSync, gulpWatch));
